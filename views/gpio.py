#!/usr/bin/env python

from flask import render_template
from manager import app
import os
from config import ON, OFF, URL


def set_low(pin):
    os.system('curl ' + URL + pin + OFF)


def set_high(pin):
    os.system('curl ' + URL + pin + ON)


@app.route('/gpio')
def gpio():
    return render_template('gpio.html')


@app.route("/gpio/pin-on/<pin>")
def gpio_pin_on(pin):
    set_high(pin)
    message = 'Pin ' + pin + ' ON!'
    templateData = {
        'pin': pin,
        'message': message
      }
    return render_template('info.html', **templateData)


@app.route("/gpio/pin-off/<pin>")
def gpio_pin_off(pin):
    set_low(pin)
    message = 'Pin ' + pin + ' OFF!'
    templateData = {
        'pin': pin,
        'message': message
      }
    return render_template('info.html', **templateData)
