#!/usr/bin/env python

from flask import render_template, redirect
from manager import app, manager


@app.route('/')
def index():
    return redirect('gpio')


@app.errorhandler(400)
def bad_request(error):
    app.logger.error('Bad request: %s', error)
    return render_template('error/400.html'), 400


@app.errorhandler(401)
def unauthorized(error):
    app.logger.error('Unauthorized: %s', error)
    return render_template('error/401.html'), 401


@app.errorhandler(403)
def forbidden(error):
    app.logger.error('Forbidden: %s', error)
    return render_template('error/403.html'), 403


@app.errorhandler(404)
def not_found(error):
    app.logger.error('Not found: %s', request.path)
    return render_template('error/404.html'), 404


@app.errorhandler(500)
def server_error(error):
    app.logger.error('Internal Server Error: %s', request.path)
    return render_template('error/500.html'), 500
