#!/usr/bin/env python

from flask import Flask
from flask_script import Manager
import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)
manager = Manager(app)

app.secret_key = '\x87*\xfb\xfd!\xa5\x9bz\x14\xc4\xaf'
app.config.from_object('config')
app.config.from_pyfile('config.py', silent=True)

if app.debug is not True:
    from config import BASE_DIR

    file_handler = RotatingFileHandler(BASE_DIR + '/error.log', maxBytes=1024 * 1024 * 100, backupCount=20)
    file_handler.setLevel(logging.ERROR)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    file_handler.setFormatter(formatter)
    app.logger.addHandler(file_handler)

from views.base import *
from views.gpio import *


@manager.shell
def make_shell_context():
    return dict(app=app)

if __name__ == '__main__':
    manager.run()