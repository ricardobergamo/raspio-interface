#!/usr/bin/env python

import os

# APP settings
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DEBUG = True
TITLE_APP = 'RaspIO'
TITLE_NAV = 'RaspberryPI 3 GPIO Interface'

# API settings
URL = 'http://192.168.0.106:8080/api/v1/pin/'
ON = '/on'
OFF = '/off'
RES = 'reset'

PINS = ['3', '5', '7', '8', '10', '11', '12', '13', '15', '16', '18', '19', '21',
        '22', '23', '24', '26', '29', '31', '32', '33', '35', '36', '37', '38', '40']
